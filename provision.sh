#! /bin/bash
#
# Set up a LEMP environment with WordPress and wp-cli
#
PHP_VER="7.4";
PHP_EXT=(
	"req:json"
	"req:mysql|mysqli|mysqlnd"
	"req:curl"
	"req:dom"
	"req:exif"
	"req:fileinfo"
	"req:hash"
	"req:imagick"
	"req:mbstring"
	"req:openssl"
	"req:pcre"
	"req:xml"
	"req:zip"
	"bcmath"
	"filter"
	"gd"
	"iconv"
	"intl"
	"mcrypt"
	"simplexml"
	"sodium"
	"xmlreader"
	"zlib"
	"opt:ssh2"
	"opt:ftp"
	"opt:sockets"
);
WP_PATH="";
WP_LOCALE="pt_BR";
WP_PLUGINS=(
	"woocommerce"
);
WP_ADMIN_EMAIL="contato@lucilio.net";
WEBSERVER_ROOT="/var/www";
WEBSERVER_USER="";
WEBSERVER_GROUP="";
SITE_DOMAIN="";
SITE_OWNER="vagrant";
_OS="$(uname -a | grep -Eio darwin\|debian\|ubuntu\|arch | tr '[:upper:]' '[:lower:]')";
_POSTINSTALLCMDS=("");

# Setup
setup() {
	SITE_DOMAIN="${SITE_DOMAIN:-$(hostname -f)}";
	SITE_OWNER="${SITE_OWNER:-root}";
	pre_install_actions;
	install_mysql &&
	install_php &&
	install_webserver &&
	install_wordpress;
	post_install_actions;
}

# Webserver
install_webserver() {
	sudo apt-get install -y apache2 && {
		if [ ! -d "${WEBSERVER_ROOT}" ]; then
			mkdir -p "${WEBSERVER_ROOT}";
		fi
		WEBSERVER_USER="$(ps -ef | egrep '(httpd|apache2|apache)' | grep -v `whoami` | grep -v root | head -n1 | awk '{print $1}')"
		WEBSERVER_GROUP="$(id "$WEBSERVER_USER" | awk '{print gensub(/.*\((.*)\)/, "\\1", "g", $2)}')";
		a2dissite 000-default;
		install_virtualhost "${SITE_DOMAIN}" "${WEBSERVER_ROOT}/${SITE_DOMAIN}";
		add_post_install_action "systemctl restart apache2";
	}
}

install_virtualhost() {
	domain="$1";
	path="$2";
	vhost_template_path="${3:-/etc/apache2/sites-available/default-ssl.conf}";
	if [ -z "$domain" -o -z "$path" ]; then
		echo "missing arguments on virtualhost";
		exit 1;
	fi
	VHOST_WEBROOT="${WEBSERVER_ROOT}/${SITE_DOMAIN}";
	if [ ! -d "${VHOST_WEBROOT}" ]; then
		mkdir -p "${VHOST_WEBROOT}";
	fi
	chown -R "${SITE_OWNER}:${WEBSERVER_GROUP}" "${VHOST_WEBROOT}";
	chmod -R g+w "${VHOST_WEBROOT}";
	
	# create vhost conf from https/ssl template
	vhost_conf_path="/etc/apache2/sites-available/${domain}.conf";
	cp -avf "${vhost_template_path}" "${vhost_conf_path}";
	
	# remove comments from vhost
	sed -i "/\s*#.*/d" $vhost_conf_path;
	
	# DocumentRoot
	if (grep DocumentRoot $vhost_conf_path); then
		# replace servername
		sed -i "s/^\(\s*\)#\?DocumentRoot.*$/\1DocumentRoot ${path//\//\\\/}/g" $vhost_conf_path;
	else
		#fix missing NameServer
		sed -i "s/^\(\s*\)<VirtualHost.*>$/\0\n\1\1DocumentRoot ${path//\//\\\/}/" $vhost_conf_path;
	fi

	# ServerName
	if (grep NameServer $vhost_conf_path); then
		# replace servername
		sed -i "s/^\(\s*\)#\?ServerName.*$/\1ServerName ${domain}/g" $vhost_conf_path;
	else
		#fix missing NameServer
		sed -i "s/^\(\s*\)<VirtualHost.*>$/\0\n\1\1ServerName ${domain}/" $vhost_conf_path;
	fi
	
	# Force SSL
	a2enmod ssl;
	cat <<-HEREDOC > /tmp/redirect.tmp
NameVirtualHost *:80
<VirtualHost *:80>
    ServerName ${domain}
    Redirect permanent / https://${domain}/
</VirtualHost>
HEREDOC
	sed -i '1s/^\(.*\)$/cat \/tmp\/redirect.tmp;echo "\1";/e' $vhost_conf_path;
	rm -f /tmp/redirect.tmp;
	
	# Allow Override (.htaccess)
	a2enmod rewrite;
	cat <<-HEREDOC > /tmp/override.tmp
<Directory "${path}">
    AllowOverride All
</Directory>
HEREDOC
	sed -i '1s/^\(\s*)(DocumentRoot.*\)$/cat \/tmp\/rewrite.tmp;echo "\1";/e' $vhost_conf_path;
	rm -f /tmp/override.tmp;

	# allow other modules to change vhost conf content
	for hook in $WEBROOT_VHOST_CONF; do
		echo "running:" ${hook} "$vhost_conf_path"  "$domain" "$path";
		${hook} "$vhost_conf_path"  "$domain" "$path";
	done
	#activate current vhost on apache
	a2ensite "$domain";
}

# PHP
install_php() {
	if [ $_OS = "debian" ]; then
		apt-get install -y lsb-release apt-transport-https ca-certificates;
		wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg;
		echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list;
	elif [ $_OS = "ubuntu" ]; then
		sudo add-apt-repository ppa:ondrej/php-qa
		sudo apt-get update	fi
	else
		echo "operational system not valid: \"$_OS\"";
		exit 1;
	fi
	apt-get update;
	apt-get install -y "php${PHP_VER}";
	for ext in "${PHP_EXT[@]}"; do
		install_php_extension "$ext";
	done
}

install_php_extension() {
	EXT_NAMES="${1#*:}";
	EXT_CLASS="${1%:*}";
	EXT_FOUND=$(php -m | grep -oE ^[^\\[].*$ | grep -oE  "^${EXT_NAMES}$");
	if [ -n "${EXT_FOUND}" ]; then
		echo "Ignoring PHP Extension \"${EXT_FOUND}\": already installed.";
	else
		echo "Searching for PHP Extension matching ${EXT_NAMES}";
		while [ -n "${EXT_NAMES}" ]; do
			EXT_NAME=${EXT_NAMES%%|*};
			if (apt-get install -y "php${PHP_VER}-${EXT_NAME}"); then
				echo " + PHP Extension ${EXT_NAME} installed";
				EXT_NAMES="";
			else
				echo " - PHP Extension ${EXT_NAME} not found";
				EXT_NAMES=${EXT_NAMES#*|};
			fi
		done
	fi
}

# MySQL
install_mysql() {
	apt-get install -y mariadb-server;
}

install_mysql_database() {
	DATABASE="${1}";
	if [ -z "${DATABASE}" ]; then
		echo "missing database name";
		exit 1;
	fi
	USER="${2%@*}";
	if [ -z "${USER}" ]; then
		USER="root";
	fi
	HOST=${2#*@};
	if [ -z "${HOST}" -o "${HOST}" == "${USER}" ]; then
		HOST="%";
	fi
	PASSWORD="${3:-$(generate_password 5)}";
	mysql -e "CREATE DATABASE IF NOT EXISTS $DATABASE;" &&
	mysql -e "GRANT ALL PRIVILEGES ON ${DATABASE}.* TO '${USER}'@'${HOST}' IDENTIFIED BY '${PASSWORD}';" ||
	exit 1;
	echo "Created new MySQL database: ${DATABASE}";
	echo "- user: ${USER:-root}@${HOST:-@}"
	echo "- password: ${PASSWORD}";
}

# WordPRess
install_wordpress() {

	# install sendmail to manage system mailing
	apt-get install -y "${WP_MAIL_APP:-sendmail}";

	# install wp-cli globally
	cd /tmp;
	curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar;
	php wp-cli.phar --info && chmod +x wp-cli.phar && mv wp-cli.phar /usr/bin/wp;
	cd - > /dev/null;

	# set base wp-cli command with correct path
	WP_PATH="${WP_PATH:-${WEBSERVER_ROOT}/${SITE_DOMAIN}}";
	wp_cli_base_command="wp --path=${WP_PATH}";

	# download wordpress to webroot
	wp_cli_download_command="${wp_cli_base_command} core download";
	WP_VERSION="${WP_VERSION:-}";
	if [ -n "${WP_VERSION}" ]; then
		wp_cli_download_command="${wp_cli_download_command} --version=${WP_VERSION}";
	fi
	if [ -n "${WP_LOCALE}" ]; then
		wp_cli_download_command="${wp_cli_download_command} --locale=${WP_LOCALE}";
	fi
	su "${SITE_OWNER}" -c "${wp_cli_download_command}";
	chown -R :"${WEBSERVER_GROUP}" "${WP_PATH}";
	chmod -R g+w "${WP_PATH}";

	# install wordpress db
	WP_DB_NAME="$(sed -E 's/[^a-zA-Z0-9]/_/' <<< ${WP_DB_NAME:-${SITE_DOMAIN%%.*}})";
	WP_DB_USER="${WP_DB_USER:-${WP_DB_NAME}}";
	WP_DB_PASSWORD="${WP_DB_PASSWORD:-$(generate_password 5)}";
	install_mysql_database "${WP_DB_NAME}" "${WP_DB_USER}" "${WP_DB_PASSWORD}";

	# setup wp-config.php
	if ! ( su "${SITE_OWNER}" -c "${wp_cli_base_command} config path" 2&>/dev/null ); then
		su "${SITE_OWNER}" -c "${wp_cli_base_command} config create --dbname=${WP_DB_NAME} --dbuser=${WP_DB_USER} --dbpass=${WP_DB_PASSWORD}";
	fi

	# install wordpress if not installed
	if [ -z "${WP_ADMIN_USER}" ]; then
		WP_ADMIN_USER="admin";
	fi
	if [ -z "${WP_ADMIN_EMAIL}" ]; then
		WP_ADMIN_EMAIL="${WP_ADMIN_USER}@${SITE_DOMAIN}";
	fi
	if [ -z "${WP_ADMIN_PASSWORD}" ]; then
		WP_ADMIN_PASSWORD="$(generate_password)";
	fi
	su "${SITE_OWNER}" -c "${wp_cli_base_command} core install --title=${WP_SITE_TITLE:-DevEnv\ WordPress} --url=${domain} --admin_user=${WP_ADMIN_USER} --admin_email=${WP_ADMIN_EMAIL} --admin_password=${WP_ADMIN_PASSWORD}";
	add_post_install_action "\
		echo \"*** ${WP_SITE_TITLE} is ready! ***\";
		echo \"access admin panel in http://${domain}/wp-admin\";
		echo \"- login: ${WP_ADMIN_USER} <${WP_ADMIN_EMAIL}>\";
		echo \"- password: ${WP_ADMIN_PASSWORD}\";
	\"";

	# create admin user
	if ! ( su "${SITE_OWNER}" -c "${wp_cli_base_command} user get ${WP_ADMIN_USER}" 2&>/dev/null ); then
		su "${SITE_OWNER}" -c "${wp_cli_base_command} user create $WP_ADMIN_USER ${WP_ADMIN_EMAIL} --role=\"administrator\"";
		echo "missing admin user \"${WP_ADMIN_USER}\" <${WP_ADMIN_EMAIL}> created with password \"${WP_ADMIN_PASSWORD}\"";
	fi

	# install plugins
	for plugin in "${WP_PLUGINS[@]}"; do
		plugin_name=$(sed 's/^.*\/\(\w*\).*$/\1/' <<< "${plugin}");
		if (su "${SITE_OWNER}" -c "${wp_cli_base_command} plugin get ${plugin_name}"); then
			su "${SITE_OWNER}" -c "${wp_cli_base_command} plugin activate ${plugin_name}";
		else
			su "${SITE_OWNER}" -c "${wp_cli_base_command} plugin install --activate ${plugin}";
		fi
		# run registered post install callbacks ( bash functions matching "post_install_wp_plugin_${plugin_name}")
		post_install_callback_name="post_install_wp_plugin_${plugin_name}";
		if [ -n "$(type -t ${post_install_callback_name})" ]; then
			echo "found post installation action for '${plugin_name}' plugin:";
			$post_install_callback_name;
			echo "...ready";
		else
			echo "'${post_install_callback_name}' is not a shell executable";
		fi
	done
}

# Common Tasks
pre_install_actions() {
	apt-get update;
}

post_install_actions() {
	for cmd in "${_POSTINSTALLCMDS[@]}"; do
		$cmd;
	done
}

post_install_wp_plugin_woocommerce() {
	if [ -z "${WP_WC_NOSAMPLEDATA}" ]; then
		wp_cli_base_command="wp --path=${WP_PATH}";
		wc_path=$(dirname $( su ${SITE_OWNER} -c "$wp_cli_base_command plugin path woocommerce" ));
		wc_sample_data_path="${wc_path}/sample-data/sample_products.xml";
		if [ ! -f "${wc_sample_data_path}" ]; then
			echo "${wc_sample_data_path} does not exists";
			exit 0;
		fi
		echo "checking for wp-importer plugin";
		su "${SITE_OWNER}" -c "wp --path=${WP_PATH} plugin install wordpress-importer --activate";
		echo "importing sample data for woocommerce from ${wc_sample_data_path}";
		su "${SITE_OWNER}" -c "wp --path=${WP_PATH} import ${wc_sample_data_path} --authors=create";
	fi
}

add_post_install_action() {
	cmd="$1";
	if [ -z "${cmd}" ]; then
		echo "Command can't be empty";
		exit 1;
	fi
	_POSTINSTALLCMDS=${_POSTINSTALLCMDS[@]/${cmd}/}; # remove existing occurrence of same command
	_POSTINSTALLCMDS+=("$cmd"); # add command to end of queue
}

generate_password() {
	LENGTH="${1:-8}";
	echo "$(date +%s | shasum | base64 | head -c $LENGTH)";
}

# Run
setup;